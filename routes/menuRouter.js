const express = require('express')
const menuRouter = express.Router()

menuRouter.route('/')
// route(/) sebagai endpoint induk
.get((req,res,next) => {
    res.end('THis is Get Module');
})

.post((req,res,next)=>{
    res.end('We will add menu:' + req.body.name + 'details:'+req.body.description);
})

.put((req,res,next)=>{
    res.statusCode = 403;
    res.end('hello');
})

.delete((req,res,next)=>{
    res.end('We will add menu:');
})

module.exports = menuRouter