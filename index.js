const express = require('express')
const app = express()
const port = 3000
const morgan = require('morgan')

const restaurant =require('./routes/restaurant')



app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.use(morgan('dev'));
app.use(express.json());

app.use('/restaurant', restaurant);